bundle: lambda.zip
.PHONY: bundle

lambda.zip: lambda.py
	zip $@ $<

clean:
	rm lambda.zip
.PHONY: clean
