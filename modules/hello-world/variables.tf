variable "prefix" {
  description = "name prefix to apply to each resource inside the module"
}

variable "schedule_expression" {
  description = "schedule expression used to define how often is the hello-world lambda is triggered"

  # For more information about the schdule expression format, see:
  # https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html
  default = "cron(* * * * ? *)"
}
