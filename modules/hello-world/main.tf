data "aws_region" "current" {
}

resource "aws_cloudwatch_event_target" "hello_world" {
  rule = aws_cloudwatch_event_rule.trigger_lambda.id
  arn  = aws_lambda_function.hello_world.arn
}

resource "aws_cloudwatch_event_rule" "trigger_lambda" {
  name                = "${var.prefix}-trigger-hello-world-on-schedule"
  schedule_expression = var.schedule_expression
  depends_on          = [aws_lambda_function.hello_world]
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hello_world.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.trigger_lambda.arn
}

resource "aws_lambda_function" "hello_world" {
  # NOTE: In a real-world scenario we would point here to an s3_bucket (url) or image_uri
  filename      = "${path.root}/lambda.zip"
  function_name = "${var.prefix}-hello-world-${data.aws_region.current.name}"
  role          = aws_iam_role.hello_world.arn
  handler       = "lambda.lambda_handler"
  runtime       = "python3.9"
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.hello_world.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

resource "aws_iam_role" "hello_world" {
  name = "${var.prefix}-hello-world-lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "lambda_logging" {
  name        = "${var.prefix}-lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}
