# Hello world project

## Requirements

- Terraform (>= 1.3) installed
- AWS CLI v2 installed
- AWS credentials configured
- AWS permissions to deploy: lambda, cloudwatch events, iam policies and roles.
- GNU-make
- zip

## Usage

1. Bundle the application: `make bundle`
    a. This will create a `lambda.zip` that we will use to deploy our lambda
2. Initialize terraform to download the required plugins: `terraform init`
2. Review the resources to be deployed and deploy: `terraform apply`
3. After the apply is done successfully we can verify that the lambda is running by:
    a. Going to [AWS log-insights](https://eu-central-1.console.aws.amazon.com/cloudwatch/home?region=eu-central-1#logsV2:logs-insights)
    b. Selecting the `/aws/lambda/dev-hello-world-eu-central-1` log group
    c. Running the following query:
```
fields @timestamp, @message, @logStream, @log
| sort @timestamp desc
| limit 20
```
  d. The `Hello SDA!` should appear in the logs

## Development

The terraform files follow the `terraform fmt` formatting standard. This is also verified as a CI step in every commit.

## Versioning

### ToDo

The repo will follow the semantic-release process where each commit is one of: fix, feature and breaking-change. For each of these the corresponding version is increased:
  fix: patch version is increased (e.g. 1.0.0 to 1.0.1)
  feature: minor version is increased (e.g. 1.0.1 to 1.1.0)
  breaking-change: major version is increase (e.g. 1.0.0 to 2.0.0)

To guarantee that the commit messages follow the semantic-relase convention all changes done to the project will be via Pull-requests and a CI job will verify all the commits in the pull request.

When merging to the main branch a semantic-release job will be run to verify if there are new versions to deploy and create a git tag if needed. This tag then will be usable when using the module like:

```terraform
module "app" {
  source = "git::https://gitlab.com/GerardoGR/sda-hello-word.git//modules/hello-world?ref=v1.0.0"

  prefix = "dev"
}
```
