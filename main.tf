provider "aws" {
  region = "eu-central-1"
}

module "app" {
  source = "./modules/hello-world"

  prefix = "dev"

  # Using the sensible default to run every minute so that we can see often that the lambda is triggered.
  # schedule_expression = "cron(* * * * * *)"
}
